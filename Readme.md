Traincraft-Localizations
========================

Localizations for Traincraft

### en_US

The en_US localization file in this repository is the main file. Use it to see what to translate. Do not edit and submit that file.

### Encoding

Files must be encoded in UTF-8. Pull requests, submissions or contributions not encoded in UTF-8 will have to be rejected or reverted.

### License

The Traincraft license applies as posted on the Minecraft forum. Modifying and/or changing lang files inside zip/jar is allowed without the written permission, but the distribution of said files is not.

### Issues

Errors in translation that you are somehow unable to make a pull request for, can be filed here. Missing localization support or code issues that affect localization can also be filled here. Bugs or reports not related to localizations on the other hand, should not be reported here and will be deleted outright.